# #!/bin/bash

# nodes=("PYTH" "CARN" "MICH" "SH1C" "STEV" "HALL")

# # Check for internet connectivity on all nodes
# for node in "${nodes[@]}"
# do
#   sudo ../connect_to.sh $node << END
#   if ip -6 route | grep  "default" >> /dev/null 2>&1;   then
#     echo "$node has internet access"
#  else
#     echo "$node does not have internet access"
#  fi
# exit
# END
# done

# # Check for export of network prefix on PYTH and HALL
# bgp_peers=("PYTH" "HALL")
# for node in "${bgp_peers[@]}"
# do
#   sudo ../connect_to.sh $node << END
#   if ip -6 route | grep  "unreachable" >> /dev/null 2>&1;   then
#     echo "$node announced the prefix over bgp"
#  else
#     echo "$node did not annouce prefix over bgp"
#  fi
# exit
# END
# done

# # Check if all routers know all the routes to all the other routers
# for node in "${nodes[@]}"
# do
#   sudo ../connect_to.sh $node << END
# if ip -6 route | grep "fd00:200:6::.*/127" * | sed -n -e '/:7$/s/:7$//p'; then
#     echo "$node has routes to all the nodes with prefix 200"
#       if ip -6 route | grep "fd00:300:6::.*/127" * | sed -n -e '/:7$/s/:7$//p'; then
#          echo "$node has routes to all the nodes with prefix 300"
#       else
#         echo "$node doesn't have routes to all the nodes with prefix 300"
#       fi
#  else
#     echo "$node doesn't have routes to all the nodes with prefix 200"
#  fi
# exit
# END
# done
