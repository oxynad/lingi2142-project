#!/usr/bin/python
#
#

import subprocess, sys, json, shlex, os

NULL = open(os.devnull, 'wb')


def ping_host(host, src, dst):
    result = subprocess.call(shlex.split("sudo ip netns exec {host} timeout 1 ping6 -I {src} -c 1 {dst}".format(host=host, src=src, dst=dst)),
            stdout=NULL, stderr=NULL)
    return result == 1

def ping_google(host):
    result = subprocess.call(shlex.split("sudo ip netns exec {host} timeout 1 ping6 2001:4860:4860::8888".format(host=host)),
            stdout=NULL, stderr=NULL)
    return result == 1

# check for internet connectivity on routers
def test_internet_connectivity(network):
    print('Internet test')
    tests = 0
    failed = 0

    for host in network.iteritems():
        if not ping_google(host):
            failed += 1
            print("{host} has no internet".format(host=host))

    if failed == 0:
        print("All tests succeeded!")
    else:
        print("{failed} failed tests out of {tests} tests".format(failed=failed, tests=tests))


# full mesh connectivity tests (test connectivity between all the hosts )
def test_network_connectivity(network):
    print("Connectivity test")
    tests = 0
    failed = 0

    for src_host, src_ips in network.iteritems():
        for src_ip in src_ips:
            for dst_host, dst_ips in network.iteritems():
                for dst_ip in dst_ips:
                    tests += 1
                    if not ping_host(src_host, src_ip, dst_ip):
                        failed += 1
                        print("{src} failed to reach {dst}".format(src=src_ip, dst=dst_ip))

    if failed == 0:
        print("All tests succeeded!")
    else:
        print("{failed} failed tests out of {tests} tests".format(failed=failed, tests=tests))



if __name__ == "__main__":

    network = {}

    file = open('network.json')
    config = json.load(file)

    for host in config.iterkeys():
        ips = []

        for AS in range(2):
            for type in range(3):
                ips.append("fd00:{AS}:2:{type}{location}{subnet}".format(AS=(200 if AS==0 else 300),type=type,location=config[host]["location"],subnet="00"))
        network[host] = ips

    test_internet_connectivity(network)
    test_network_connectivity(network)