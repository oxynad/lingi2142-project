#!/bin/bash

#up ports
up_ports = "1024:65535"

for X in 200 300
    do
        #ssh connection allow only for staff and student
        ip6tables -A INPUT -p tcp --dport 22 --sport $up_ports -s fd00:X:6:1100::/64 -j LOG_ACCEPT
        ip6tables -A INPUT -p tcp --dport 22 --sport $up_ports -s fd00:X:6:2100::/64 -j LOG_ACCEPT
        ip6tables -A OUTPUT -p tcp  --sport 22 -d fd00:$X:6:2100::/64 --dport $up_ports ! -syn -j LOG_ACCEPT
        ip6tables -A OUTPUT -p tcp --sport 22 -d fd00:$X:6:2100::/64 --dport $up_ports ! -syn -j LOG_ACCEPT
        ip6tables -A INPUT -p icmp -j ACCEPT

        #ICMP restriction
        ip6tables -A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 128 -m limit --limit 10/min -j ACCEPT
        ip6tables -A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 143 -j ACCEPT
        ip6tables -A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 135 -j ACCEPT
        ip6tables -A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 133 -j DROP
        iptables -A FORWARD -p  ipv6-icmp -j ACCEPT

        #deny access to staff for student 
        ip6tables -A INPUT -s fd00:$X:6:1100::/64  -j DROP  

   done


